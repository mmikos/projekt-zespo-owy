﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts
{
    [DataContract]
    public class EntryCategoriesFilterContract
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int? CategoryId { get; set; }

        [DataMember]
        public int? EntryId { get; set; }
    }
}
