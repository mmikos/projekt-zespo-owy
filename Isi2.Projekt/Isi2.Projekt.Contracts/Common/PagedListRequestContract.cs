using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts.Common
{
    [DataContract]
    public class PagedListRequestContract
    {
        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int Take { get; set; }
    }
}
