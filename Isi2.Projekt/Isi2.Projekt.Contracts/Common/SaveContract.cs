
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Isi2.Projekt.Contracts.Common
{
    [DataContract]
    public class SaveContract
    {
        [DataMember]
        public string Errors { get; set; }

        [DataMember]
        public bool Success { get; set; }

    }
}
